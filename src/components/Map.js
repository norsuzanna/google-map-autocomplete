import React, { useEffect, useRef } from "react";
import "./component.css";

const Map = ({ center, zoom }) => {
  const ref = useRef();

  useEffect(() => {
    new window.google.maps.Map(ref.current, { center, zoom });
    let map = new window.google.maps.Map(ref.current, { center, zoom });
    let marker = new window.google.maps.Marker({
      position: center
    });

    marker.setMap(map);
  }, [ref, zoom, center]);

  return <div ref={ref} className="map" />;
};

export default Map;
