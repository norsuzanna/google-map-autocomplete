import React, { useRef } from "react";
import { Input } from "antd";
import { usePlacesWidget } from "react-google-autocomplete";
import { useDispatch } from "react-redux";
import { setPlaces, selectedPlace } from "../redux/actions/placeActions";
import "./component.css";

const SearchField = () => {
  const dispatch = useDispatch();
  const antInputRef = useRef(null);
  const { ref: antRef } = usePlacesWidget({
    apiKey: "AIzaSyBQ_Tps_6uIvQAvKH5LtVVP9U99N3gVCaE",
    onPlaceSelected: (place) => {
      dispatch(setPlaces(place));
      dispatch(selectedPlace(place));
    },
  });

  return (
    <Input
      ref={(c) => {
        antInputRef.current = c;
        if (c) antRef.current = c.input;
      }}
      placeholder="Enter a place"
    />
  );
};

export default SearchField;
