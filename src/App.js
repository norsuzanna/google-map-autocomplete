import "./App.css";
import "./components/component.css";
import { Wrapper, Status } from "@googlemaps/react-wrapper";
import { useSelector } from "react-redux";
import Map from "./components/Map";
import SearchField from "./components/SearchField";
import SearchResult from "./components/SearchResult";

let center = { lat: 3.0569001741909085, lng: 101.47287202676856 };
let zoom = 18;

const render = (status) => {
  if (status === Status.LOADING) return <h3>{status} ..</h3>;
  if (status === Status.FAILURE) return <h3>{status} ...</h3>;
  return null;
};

function App() {
  let selectedPlace = useSelector((state) => state.allPlaces.selectedPlace);
  let { lat, lng } = selectedPlace;

  return (
    <div className="general-layout">
      <div className="map-wrapper">
        <Wrapper
          render={render}
          apiKey={"AIzaSyBQ_Tps_6uIvQAvKH5LtVVP9U99N3gVCaE"}
        >
          <Map
            zoom={zoom}
            center={lat === undefined ? center : { lat: lat, lng: lng }}
          />
        </Wrapper>
      </div>
      <div className="search-layout">
        <SearchField />
        <SearchResult />
      </div>
    </div>
  );
}

export default App;
